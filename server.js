const express = require("express");
const socket = require("socket.io");
const {MongoClient} = require('mongodb');

// App setup
const PORT = 7000;
const app = express();

// MongoDB connection URL
const MONGO_DB_URL = `mongodb+srv://root:3yJ4nCbSaBAn87Fn@testone.vr8tl.mongodb.net/databoxes?retryWrites=true&w=majority`;
const MONGO_DB_NAME = 'databoxes';
const MONGO_DB_COLLECTION = 'client_replies';

const server = app.listen(PORT, () => {
  console.log("server is running on port", PORT);
});

// Use connect method to connect to the server
const MgDBclient = new MongoClient(MONGO_DB_URL, { useUnifiedTopology: true });
try {
  MgDBclient.connect();
  console.log("MongoDB connected successfully to server");
} catch (e) {
  console.error(e);
} finally {
  MgDBclient.close();
}

app.get('/', function (request, response) {
  response.send(`Socket server started on port ${PORT}, waiting for clients...`)
});

// Static files
app.use(express.static(__dirname));

// Socket setup
const io = socket(server);

const activeConnections = new Set();
const clientReplies = new Set();

io.on("connection", (socket) => {

  // New incoming client listener
  socket.on("new_client", (data) => {
    socket.client_id = data.id;
    activeConnections.add(data);
    socket.emit("new_client", `Hello world. ${data.name}`);
  });

  // New incoming replies listener
  socket.on("new_message", (data) => {
    clientReplies.add(data);
    socket.emit("new_message", `Thanks for your reply.`);

    // Check to get replies from all clients
    clientRepliesIdArray = [...clientReplies].map((reply) => reply.id )
    if(clientRepliesIdArray.length == activeConnections.size){
      // Save to database
      MgDBclient.db(MONGO_DB_NAME).collection(MONGO_DB_COLLECTION).insertOne(data);
      console.log(`Messages has been saved successfully.`);
    }

  });

  // Listen to client connection status and update "ActiveConnections set"
  socket.on("client_disconnected", () => {
    activeConnections.delete(socket.client_id);
    socket.emit("client_disconnected", socket.client_id);
  });

});